import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { ServiciosRoutingModule, routedComponents } from './servicios-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    ServiciosRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class ServiciosModule { }
