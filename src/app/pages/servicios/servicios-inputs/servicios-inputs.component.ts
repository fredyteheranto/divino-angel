import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;

@Component({
  selector: 'ngx-servicios-inputs',
  styleUrls: ['./servicios-inputs.component.scss'],
  templateUrl: './servicios-inputs.component.html',
})
export class ServiciosInputsComponent {
	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public estadoBoton

  starRate = 2;
  heartRate = 4;

  NuevaServicio(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'
	}
	GavaServicio(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanServicio(){
		this.DisplayFormulario = false
		this.OcultaTabla= true

	}
	borraServicio(){
		swal({
			  title: 'Eliminar Servicio',
			  text: "Desea Borrar a esta Servicio",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la Servicio.',
			      'success'
			    )
			  }
		})
	}

	gteServicioactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}
