import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { serviciossComponent } from './servicios.component';
import { ServiciosInputsComponent } from './servicios-inputs/servicios-inputs.component';
import { ServiciosLayoutsComponent } from './servicios-layouts/servicios-layouts.component';

const routes: Routes = [{
  path: '',
  component: serviciossComponent,
  children: [{
    path: 'servicios-index',
    component: ServiciosInputsComponent,
  }, {
    path: 'padron-servicios',
    component: ServiciosLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ServiciosRoutingModule {

}

export const routedComponents = [
  serviciossComponent,
  ServiciosInputsComponent,
  ServiciosLayoutsComponent,
];
