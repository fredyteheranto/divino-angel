import { Component } from '@angular/core';

@Component({
  selector: 'ngx-servicios-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class serviciossComponent {
}
