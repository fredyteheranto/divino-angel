import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { ReportesRoutingModule, routedComponents } from './reportes-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    ReportesRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class ReportesModule { }
