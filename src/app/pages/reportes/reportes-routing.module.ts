import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { reportessComponent } from './reportes.component';
import { ReportesInputsComponent } from './reportes-inputs/reportes-inputs.component';
import { ReportesLayoutsComponent } from './reportes-layouts/reportes-layouts.component';

const routes: Routes = [{
  path: '',
  component: reportessComponent,
  children: [{
    path: 'crear-reportes',
    component: ReportesInputsComponent,
  }, {
    path: 'padron-reportes',
    component: ReportesLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportesRoutingModule {

}

export const routedComponents = [
  reportessComponent,
  ReportesInputsComponent,
  ReportesLayoutsComponent,
];
