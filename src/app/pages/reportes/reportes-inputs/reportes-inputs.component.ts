import { Component } from '@angular/core';

@Component({
  selector: 'ngx-reportes-inputs',
  styleUrls: ['./reportes-inputs.component.scss'],
  templateUrl: './reportes-inputs.component.html',
})
export class ReportesInputsComponent {

  starRate = 2;
  heartRate = 4;
}
