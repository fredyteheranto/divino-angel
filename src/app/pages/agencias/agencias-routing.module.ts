import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { agenciasComponent } from './agencias.component';
import { AgenciasInputsComponent } from './agencias-inputs/agencias-inputs.component';
import { AgenciasLayoutsComponent } from './agencias-layouts/agencias-layouts.component';

const routes: Routes = [{
  path: '',
  component: agenciasComponent,
  children: [{
    path: 'agencias-index',
    component: AgenciasInputsComponent,
  }, {
    path: 'padron-agencias',
    component: AgenciasLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class AgenciasRoutingModule {

}

export const routedComponents = [
  agenciasComponent,
  AgenciasInputsComponent,
  AgenciasLayoutsComponent,
];
