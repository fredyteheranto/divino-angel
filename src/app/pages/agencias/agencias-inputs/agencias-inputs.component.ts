import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;

@Component({
  selector: 'ngx-agencias-inputs',
  styleUrls: ['./agencias-inputs.component.scss'],
  templateUrl: './agencias-inputs.component.html',
})
export class AgenciasInputsComponent {

	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public estadoBoton

  starRate = 2;
  heartRate = 4;
  NuevaAgencia(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'  	
  }
  GavaAgencia(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanAgencia(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
		borraAgencia(){
		swal({
			  title: 'Eliminar Agencia',
			  text: "Desea Borrar a esta Agencia",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la Agencia.',
			      'success'
			    )
			  }
		})
	}

	gteAgenciaactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}
