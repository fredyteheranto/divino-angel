import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { AgenciasRoutingModule, routedComponents } from './agencias-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    AgenciasRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class AgenciasModule { }
