import { Component } from '@angular/core';

@Component({
  selector: 'ngx-agencias-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class agenciasComponent {
}
