import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, 
  {
    path: 'personas',
    loadChildren: './personas/personas.module#PersonasModule',
  }, 
  {
    path: 'servicios',
    loadChildren: './servicios/servicios.module#ServiciosModule', 
  },
  {
    path: 'agencias',
    loadChildren: './agencias/agencias.module#AgenciasModule', 
  },
  {
    path: 'buses',
    loadChildren: './buses/buses.module#BusesModule',
  },
  {
    path: 'rutas',
    loadChildren: './rutas/rutas.module#RutasModule',
  },
  {
    path: 'itinerarios',
    loadChildren: './itinerarios/itinerarios.module#ItinerariosModule',
  },{
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  },{
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
