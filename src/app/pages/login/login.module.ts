import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { LoginRoutingModule, routedComponents } from './login-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    LoginRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class LoginModule { }
