import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;

@Component({
  selector: 'ngx-login-inputs',
  styleUrls: ['./login-inputs.component.scss'],
  templateUrl: './login-inputs.component.html',
})
export class LoginInputsComponent {

	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public estadoBoton

  starRate = 2;
  heartRate = 4;
  NuevaLogin(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'  	
  }
  GavaLogin(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanLogin(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
		borraLogin(){
		swal({
			  title: 'Eliminar Login',
			  text: "Desea Borrar a esta Login",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la Login.',
			      'success'
			    )
			  }
		})
	}

	gteLoginactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}
