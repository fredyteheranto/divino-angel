import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { loginComponent } from './login.component';
import { LoginInputsComponent } from './login-inputs/login-inputs.component';
import { LoginLayoutsComponent } from './login-layouts/login-layouts.component';

const routes: Routes = [{
  path: '',
  component: loginComponent,
  children: [{
    path: 'entrar',
    component: LoginInputsComponent,
  }, {
    path: 'padron-login',
    component: LoginLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class LoginRoutingModule {

}

export const routedComponents = [
  loginComponent,
  LoginInputsComponent,
  LoginLayoutsComponent,
];
