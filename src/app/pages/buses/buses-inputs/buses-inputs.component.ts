import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;


@Component({
  selector: 'ngx-buses-inputs',
  styleUrls: ['./buses-inputs.component.scss'],
  templateUrl: './buses-inputs.component.html',
})
export class BusesInputsComponent {

	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public estadoBoton
	public ModeloBus
	public Marca
	public Placa
	public FAdquisicion
	public NSerieMotor
	public FFabricacion
	public Asientos
	public Estado
	public SOAT
	public CaducaSoat
	public TUCVigente
	public CaducaTUC

  starRate = 2;
  heartRate = 4;

  NuevoBus(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'
	}

	GavaBus(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanBus(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
		borraBuses(){
		swal({
			  title: 'Eliminar Bus',
			  text: "Desea Borrar a esta Bus",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la Bus.',
			      'success'
			    )
			  }
		})
	}

	gteBusesactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}
