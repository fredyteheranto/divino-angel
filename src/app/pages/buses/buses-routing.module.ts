import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { busessComponent } from './buses.component';
import { BusesInputsComponent } from './buses-inputs/buses-inputs.component';
import { BusesLayoutsComponent } from './buses-layouts/buses-layouts.component';

const routes: Routes = [{
  path: '',
  component: busessComponent,
  children: [{
    path: 'crear-buses',
    component:  BusesLayoutsComponent,
  }, {
    path: 'padron-buses',
    component: BusesInputsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class BusesRoutingModule {

}

export const routedComponents = [
  busessComponent,
  BusesInputsComponent,
  BusesLayoutsComponent,
];
