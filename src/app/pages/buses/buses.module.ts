import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { BusesRoutingModule, routedComponents } from './buses-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    BusesRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class BusesModule { }
