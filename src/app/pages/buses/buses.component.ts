import { Component } from '@angular/core';

@Component({
  selector: 'ngx-buses-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class busessComponent {
}
