import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Escritorio',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Buses',
    icon: 'nb-shuffle',
    link: '/pages/buses/padron-buses',
    
  },
  {
    title: 'Itinerarios',
    icon: 'nb-compose',
    link: '/pages/itinerarios/itinerarios-index',
   
  },
  {
    title: 'Servicios',
    icon: 'nb-gear',
    link: '/pages/servicios/servicios-index',
    
  },
  {
    title: 'Agencias',
    icon: 'nb-gear',
    link: '/pages/agencias/agencias-index',
    
  },
  {
    title: 'Rutas',
    icon: 'nb-location',
    link: '/pages/rutas/rutas-index',
    
  },
  {
    title: 'Reportes',
    icon: 'nb-bar-chart',
    
  },
  
  {
    title: 'Personas',
    icon: 'nb-person',
    link: '/pages/personas/padron-personas',
   
  },
  {
    title: 'Cuadres De Caja',
    icon: 'nb-tables',
  
  }
];
