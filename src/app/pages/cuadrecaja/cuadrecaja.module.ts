import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { CuadrecajaRoutingModule, routedComponents } from './cuadrecaja-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    CuadrecajaRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class CuadrecajaModule { }
