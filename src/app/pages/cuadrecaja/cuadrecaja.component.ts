import { Component } from '@angular/core';

@Component({
  selector: 'ngx-cuadrecaja-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class cuadrecajasComponent {
}
