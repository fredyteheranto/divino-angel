import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { cuadrecajasComponent } from './cuadrecaja.component';
import { CuadrecajaInputsComponent } from './cuadrecaja-inputs/cuadrecaja-inputs.component';
import { CuadrecajaLayoutsComponent } from './cuadrecaja-layouts/cuadrecaja-layouts.component';

const routes: Routes = [{
  path: '',
  component: cuadrecajasComponent,
  children: [{
    path: 'crear-cuadrecaja',
    component: CuadrecajaInputsComponent,
  }, {
    path: 'padron-cuadrecaja',
    component: CuadrecajaLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class CuadrecajaRoutingModule {

}

export const routedComponents = [
  cuadrecajasComponent,
  CuadrecajaInputsComponent,
  CuadrecajaLayoutsComponent,
];
