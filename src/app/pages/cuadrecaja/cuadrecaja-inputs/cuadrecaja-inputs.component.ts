import { Component } from '@angular/core';

@Component({
  selector: 'ngx-cuadrecaja-inputs',
  styleUrls: ['./cuadrecaja-inputs.component.scss'],
  templateUrl: './cuadrecaja-inputs.component.html',
})
export class CuadrecajaInputsComponent {

  starRate = 2;
  heartRate = 4;
}
