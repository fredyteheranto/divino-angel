import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;

@Component({
  selector: 'ngx-personas-inputs',
  styleUrls: ['./personas-inputs.component.scss'],
  templateUrl: './personas-inputs.component.html',
})
export class PersonasInputsComponent {

	starRate = 2;
	heartRate = 4;
	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public Pnatural :boolean = true
	public Pjuridica :boolean = false
	public tipopersona
	public documento
	public numero
	public apepaterno
	public apematerno
	public nombres
	public fchaNacimeinto
	public sexo
	public estadocivil
	public Rsocial
	public giro
	public contacto
	public direccion
	public departamento
	public provincia
	public distrito
	public email
	public obsev
	public licencia
	public fchaalta
	public estadoBoton


ngOnInit() {
	console.log('s')


    }
 
 verificaPersona(tipopersona){
 	if (tipopersona == 'N') {
 		this.Pnatural= true
 		this.Pjuridica= false
 	}
 	if (tipopersona == 'J') {
 		this.Pnatural= false
 		this.Pjuridica= true
 	}

 }
  NuevaPersona(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'
	}
	GavaPersona(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanPersona(){
		this.DisplayFormulario = false
		this.OcultaTabla= true

	}
	borraPersona(){
		swal({
			  title: 'Eliminar persona',
			  text: "Desea Borrar a esta persona",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la persona.',
			      'success'
			    )
			  }
		})
	}

	gtePersonaactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}


