import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { PersonasRoutingModule, routedComponents } from './personas-routing.module';



@NgModule({
  imports: [
    ThemeModule,
    PersonasRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class PersonasModule { }
