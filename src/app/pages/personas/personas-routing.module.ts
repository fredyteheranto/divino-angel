import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { personassComponent } from './personas.component';
import { PersonasInputsComponent } from './personas-inputs/personas-inputs.component';
import { PersonasLayoutsComponent } from './personas-layouts/personas-layouts.component';

const routes: Routes = [{
  path: '',
  component: personassComponent,
  children: [{
    path: 'padron-personas',
    component: PersonasInputsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class PersonasRoutingModule {

}

export const routedComponents = [
  personassComponent,
  PersonasInputsComponent,
  PersonasLayoutsComponent,
];
