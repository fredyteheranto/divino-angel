import { Component } from '@angular/core';

@Component({
  selector: 'ngx-rutas-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class rutassComponent {
}
