import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;

@Component({
  selector: 'ngx-rutas-inputs',
  styleUrls: ['./rutas-inputs.component.scss'],
  templateUrl: './rutas-inputs.component.html',
})
export class RutasInputsComponent {

  starRate = 2;
  heartRate = 4;
	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public estadoBoton
	 NuevaRuta(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'  	
  }
  GavaRuta(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanRuta(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
		borraRuta(){
		swal({
			  title: 'Eliminar Ruta',
			  text: "Desea Borrar a esta Ruta",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la Ruta.',
			      'success'
			    )
			  }
		})
	}

	gteRutaactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}
