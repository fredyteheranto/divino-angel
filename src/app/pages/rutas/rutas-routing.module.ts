import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { rutassComponent } from './rutas.component';
import { RutasInputsComponent } from './rutas-inputs/rutas-inputs.component';
import { RutasLayoutsComponent } from './rutas-layouts/rutas-layouts.component';

const routes: Routes = [{
  path: '',
  component: rutassComponent,
  children: [{
    path: 'rutas-index',
    component: RutasInputsComponent,
  }, {
    path: 'padron-rutas',
    component: RutasLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class RutasRoutingModule {

}

export const routedComponents = [
  rutassComponent,
  RutasInputsComponent,
  RutasLayoutsComponent,
];
