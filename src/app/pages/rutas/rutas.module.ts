import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { RutasRoutingModule, routedComponents } from './rutas-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    RutasRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class RutasModule { }
