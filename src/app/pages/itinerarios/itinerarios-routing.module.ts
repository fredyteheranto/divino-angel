import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { itinerariossComponent } from './itinerarios.component';
import { ItinerariosInputsComponent } from './itinerarios-inputs/itinerarios-inputs.component';
import { ItinerariosLayoutsComponent } from './itinerarios-layouts/itinerarios-layouts.component';

const routes: Routes = [{
  path: '',
  component: itinerariossComponent,
  children: [{
    path: 'itinerarios-index',
    component: ItinerariosInputsComponent,
  }, {
    path: 'padron-itinerarios',
    component: ItinerariosLayoutsComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ItinerariosRoutingModule {

}

export const routedComponents = [
  itinerariossComponent,
  ItinerariosInputsComponent,
  ItinerariosLayoutsComponent,
];
