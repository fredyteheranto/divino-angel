import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import swal from 'sweetalert2'
declare var $: any;


@Component({
  selector: 'ngx-itinerarios-inputs',
  styleUrls: ['./itinerarios-inputs.component.scss'],
  templateUrl: './itinerarios-inputs.component.html',
})
export class ItinerariosInputsComponent {

  starRate = 2;
  heartRate = 4;
  	public DisplayFormulario:boolean = false
	public OcultaTabla:boolean = true
	public estadoBoton

	 NuevoInireario(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Guardar'
	}

	GavaInireario(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
	CanInireario(){
		this.DisplayFormulario = false
		this.OcultaTabla= true
	}
		borraInirearios(){
		swal({
			  title: 'Eliminar Itinerario',
			  text: "Desea Borrar a esta Itinerario",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Borrar'
			}).then((result) => {
			  if (result.value) {
			    swal(
			      'Borrado!',
			      'Se ha borrado la Itinerario.',
			      'success'
			    )
			  }
		})
	}

	gteInireariosactuliza(){
		this.DisplayFormulario = true
		this.OcultaTabla= false
		this.estadoBoton = 'Actualizar'
	}
}

