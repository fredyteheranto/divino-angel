import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { ItinerariosRoutingModule, routedComponents } from './itinerarios-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    ItinerariosRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class ItinerariosModule { }
