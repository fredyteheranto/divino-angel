import { Component } from '@angular/core';

@Component({
  selector: 'ngx-itinerarios-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class itinerariossComponent {
}
