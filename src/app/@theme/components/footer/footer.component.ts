import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Creado con ♥ por <b><a href="https://www.workana.com/freelancer/intechsof" target="_blank">Fteheran</a></b> 2018</span>
    
  `,
})
export class FooterComponent {
}
